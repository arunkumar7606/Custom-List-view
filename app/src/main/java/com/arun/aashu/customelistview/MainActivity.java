package com.arun.aashu.customelistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;

//    Integer picture[]={
//
//            R.drawable.kbc,
//            R.drawable.jaishankar,
//            R.drawable.kabir,
//            R.drawable.kumarvishwas,
//            R.drawable.prem,
//            R.drawable.ramdhari,
//            R.drawable.tulsidas,
//            R.drawable.ic_launcher_background,
//
//    };

//    String text[]={
//            "alfa",
//            "beta",
//            "cupcake",
//            "donut",
//            "Eclear",
//            "Foryo",
//            "GingerBread",
//            "HonyComb"
//    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView=findViewById(R.id.mylist);

//        MyAdapter adapter=new MyAdapter(this, text, picture);

        ArrayList<MyDataPojo> dabbaObj=new ArrayList<MyDataPojo>();

        MyDataPojo khana1Obj=new MyDataPojo("aashu",R.drawable.kbc);
        MyDataPojo khana1Obj1=new MyDataPojo("vishu",R.drawable.jaishankar);
        MyDataPojo khana1Obj2=new MyDataPojo("harman",R.drawable.kabir);
        MyDataPojo khana1Obj3=new MyDataPojo("rahul",R.drawable.kumarvishwas);
        MyDataPojo khana1Obj4=new MyDataPojo("aman",R.drawable.prem);
        MyDataPojo khana1Obj5=new MyDataPojo("puneet",R.drawable.ramdhari);
        MyDataPojo khana1Obj6=new MyDataPojo("manish",R.drawable.tulsidas);
        MyDataPojo khana1Obj7=new MyDataPojo("mehak",R.drawable.ic_launcher_background);

        dabbaObj.add(khana1Obj);
        dabbaObj.add(khana1Obj1);
        dabbaObj.add(khana1Obj2);
        dabbaObj.add(khana1Obj3);
        dabbaObj.add(khana1Obj4);
        dabbaObj.add(khana1Obj5);
        dabbaObj.add(khana1Obj6);
        dabbaObj.add(khana1Obj7);

        MyAdapter adapter=new MyAdapter(this,dabbaObj);

        listView.setAdapter(adapter);








    }
}
