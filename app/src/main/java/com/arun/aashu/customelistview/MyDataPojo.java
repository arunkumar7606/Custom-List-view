package com.arun.aashu.customelistview;

import android.widget.ImageView;

/**
 * Created by aAsHu on 3/23/2018.
 */

public class MyDataPojo {



    String text;
    Integer img;

    public MyDataPojo(String text) {
        this.text = text;
    }


    public MyDataPojo(String text, Integer img) {
        this.text = text;
        this.img = img;
    }

    public MyDataPojo() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getImg() {
        return img;
    }

    public void setImg(Integer img) {
        this.img = img;
    }


}
