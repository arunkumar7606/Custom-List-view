package com.arun.aashu.customelistview;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aAsHu on 3/23/2018.
 */

public class MyAdapter extends ArrayAdapter<MyDataPojo> {

    private final LayoutInflater inflater;
//
//    Integer pic[];
//    String txt[];
//    Activity activity;

    ArrayList<MyDataPojo> arrayList = new ArrayList<>();
    Activity context;


    public MyAdapter(Activity context, ArrayList<MyDataPojo> arrayList) {

        super(context, 0, arrayList);
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arrayList = arrayList;

    }

//    public MyAdapter(Activity activity, String txt[], Integer pic[]){
//
//        super(activity,R.layout.jadu,txt);
//        this.pic=pic;
//        this.txt=txt;
//        this.activity=activity;
//    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {

            /****** Inflate jadu.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.jadu, null);


            ImageView imageView = view.findViewById(R.id.imageview);
            TextView textView = view.findViewById(R.id.textview);


            imageView.setImageResource(arrayList.get(position).getImg());
            textView.setText(arrayList.get(position).getText());

        }
        return view;
    }

}

// public class MyAdapter extends BaseAdapter {
//
//    /*********** Declare Used Variables *********/
//
//    private static LayoutInflater inflater=null;
//    private final Activity activity;
//    private final ArrayList<MyDataPojo> data;
//
//
//    /*************  CustomAdapter Constructor *****************/
//    public MyAdapter(Activity a, ArrayList<MyDataPojo> d) {
//
//        /********** Take passed values **********/
//        activity = a;
//        data=d;
//
//        /***********  Layout inflator to call external xml layout () ***********/
//        inflater = ( LayoutInflater )activity.
//                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//    }
//
//    /******** What is the size of Passed Arraylist Size ************/
//    public int getCount() {
//
//        if(data.size()<=0)
//            return 1;
//        return data.size();
//    }
//
//    public Object getItem(int position) {
//        return position;
//    }
//
//    public long getItemId(int position) {
//        return position;
//    }
//
//    /********* Create a holder Class to contain inflated xml file elements *********/
//    public static class ViewHolder{
//
//        public TextView text;
//        public ImageView image;
//
//    }
//
//    /****** Depends upon data size called for each row , Create each ListView row *****/
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        View vi = convertView;
//        ViewHolder holder;
//
//        if(convertView==null){
//
//            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
//            vi = inflater.inflate(R.layout.jadu, null);
//
//            /****** View Holder Object to contain tabitem.xml file elements ******/
//
//            holder = new ViewHolder();
//            holder.text = (TextView) vi.findViewById(R.id.textview);
//            holder.image=(ImageView)vi.findViewById(R.id.imageview);
//            holder.text.setText(data.get(position).getText());
//            holder.image.setImageResource(data.get(position).getImg());
//            /************  Set holder with LayoutInflater ************/
//            vi.setTag( holder );
//        }
//        else
//            holder=(ViewHolder)vi.getTag();
//
//        if(data.size()<=0)
//        {
//            holder.text.setText("No Data");
//
//        }
//
//        return vi;
//    }
//
//
//
//  }


//
